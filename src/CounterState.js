import { observable, action } from 'mobx';

const CounterState = () => {
  const state = observable({
    count: 0,
    increment() {
      state.count += 1;
    },
    decrement() {
      state.count -= 1;
    },
  }, {
    increment: action,
    decrement: action,
  });

  return state;
};

export default CounterState;
