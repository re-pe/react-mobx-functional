import {
  div, header, img, p, code, a,
} from 'react-hyperscript-helpers';
import logo from './logo.svg';
import './App.css';

const App = () => (
  div('.App', [
    header('.App-header', [
      img('.App-logo', { src: logo, alt: 'logo' }),
      p(['Edit ', code('src/App.js'), ' and save to reload.']),
      a('.App-link', { href: 'https://reactjs.org', target: '_blank', rel: 'noopener noreferrer' }, 'Learn React'),
    ]),
  ])
);

export default App;
