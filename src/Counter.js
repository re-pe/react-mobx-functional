import { observer } from 'mobx-react';
import { div, br, button } from 'react-hyperscript-helpers';
import { createElement as Element } from 'react';

import CounterState from './CounterState';

const Counter = () => {
  const state = CounterState();
  return Element(
    observer(() => (
      div([
        'Counter: ',
        state.count,
        br(),
        button({ onClick: state.increment }, ' + '),
        button({ onClick: state.decrement }, ' - '),
      ])
    )),
  );
};

export default Counter;
