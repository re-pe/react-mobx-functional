import { render } from 'react-dom';
import './index.css';
import { div, br } from 'react-hyperscript-helpers';
import App from './App';
import Counter from './Counter';
import * as serviceWorker from './serviceWorker';

render(
  div([App(), Counter(), br(), Counter()]),
  document.getElementById('root'),
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
